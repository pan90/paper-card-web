import './App.css';
import OnlinePlayersPage from './pages/OnlinePlayersPage';
import { Routes, Route, Navigate, Link } from 'react-router-dom';
import IndexPage from './pages/IndexPage';
import { Col, Menu, Row } from 'antd';
import RulesPage from './pages/RulesPage';
import WhiteListPage from './pages/WhiteListPage';
import BlackListPage from './pages/BlackListPage';
import WhiteListAuditPage from './pages/WhiteListAuditPage';
import LoginPage from './pages/LoginPage';


function App() {

  // return <OnlinePlayersPage/>

  return <Row align={'middle'} justify={'center'}>
    <Col xs={23} sm={23} md={22} lg={20} xl={18} xxl={16} className='center-content'>
      <Menu mode={"horizontal"} >
        <Menu.Item><Link to={"/"}>首页</Link></Menu.Item>
        <Menu.Item><Link to={"login"}>登录</Link></Menu.Item>
        <Menu.Item><Link to={"online-players"}>在线玩家</Link></Menu.Item>
        <Menu.Item><Link to={"rules"}>基本条例</Link></Menu.Item>
        <Menu.Item><Link to={"whitelist"}>白名单</Link></Menu.Item>
        <Menu.Item><Link to={"whitelist-audit"}>白名单审核</Link></Menu.Item>
        <Menu.Item><Link to={"blacklist"}>黑名单</Link></Menu.Item>
      </Menu>


      <main className='main-content'>
        <Routes>
          <Route path='online-players' element={<OnlinePlayersPage />}></Route>
          <Route path='login' element={<LoginPage/>}></Route>
          <Route path='rules' element={<RulesPage />}></Route>
          <Route path='whitelist' element={<WhiteListPage />}></Route>
          <Route path='whitelist-audit' element={<WhiteListAuditPage />}></Route>
          <Route path='blacklist' element={<BlackListPage />}></Route>
          <Route path='/' element={<IndexPage />}> </Route>

          <Route path='*' element={<Navigate to={"/"} />}></Route>
        </Routes>
      </main>
    </Col>
  </Row>
}

export default App;
