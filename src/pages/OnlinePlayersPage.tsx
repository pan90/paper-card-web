import axios from "axios";
import { useCallback, useEffect, useRef, useState } from "react";


import { Avatar, Button, Card, Col, Row, Spin } from "antd";
import { Link } from "react-router-dom";



const api = axios.create({
    baseURL: "http://mc.paper-card.cn:22100/api",
    timeout: 1000
});

function playerHeadUrl(uuid: string) {
    return "https://crafatar.com/avatars/" + uuid + "?size=64&overlay";
}

type PlayerInfo = {
    name: string;
    uuid: string;
    ping: number;
    last_login_time: number;
};

function calcOnlineTime(last_login_time: number, current: number) {

    last_login_time -= 8 * 60 * 60 * 1000;

    let delta = (current - last_login_time);

    const hours = Math.floor(delta / (60 * 60 * 1000));
    delta -= hours * 60 * 60 * 1000;

    const minutes = Math.floor(delta / (60 * 1000));
    delta -= minutes * 60 * 1000;

    const seconds = Math.floor(delta / 1000);

    if (hours > 0) return `${hours}时${minutes}分${seconds}秒`;
    if (minutes > 0) return `${minutes}分${seconds}秒`;
    return `${seconds}秒`;
}

function pingColor(ping: number) {
    if (ping < 90) return "green";
    if (ping < 180) return "lightgreen";
    if (ping < 300) return "yellow";
    if (ping < 500) return "orange";
    return "red";
}


function OnlinePlayerInfo(props: { player: PlayerInfo }) {

    const { player } = props;

    // useEffect(() => {
    //     axios.get(`https://sessionserver.mojang.com/session/minecraft/profile/${player.uuid}`)
    //         .then(data => {
    //             console.log(data.data);
    //         })
    //         .catch(e => {
    //             console.error(e);
    //         })

    // }, []);

    // 当前时间戳
    const current = new Date().getTime();

    return <>
        <Card title=
            {
                <Link to={`https://minecraftuuid.com/?search=${player.uuid}`} target={"_blank"}>
                    <Avatar size={"large"} shape={"square"}
                        src={playerHeadUrl(player.uuid)} />
                    <span>{player.name}</span>
                </Link>
            }>
            <p>Ping: <span style={{ color: pingColor(player.ping) }} >{player.ping}</span></p>
            <p>在线时长：{calcOnlineTime(player.last_login_time, current)}</p>
            <p>UUID: {player.uuid}</p>
        </Card>
    </>
}


function OnlinePlayersPage() {

    const [players, setPlayers] = useState([] as PlayerInfo[]);
    const [refreshCount, setRefreshCount] = useState(0);

    // 获取第一页
    useEffect(() => {

        api.get("/online-players")
            .then((data) => {
                const d = data.data as PlayerInfo[];

                d.sort((a, b) => {
                    return a.last_login_time - b.last_login_time;
                })

                setPlayers(d);
            })
            .catch(e => {
                console.error(e);
            });

        return () => {
        }

    }, [refreshCount]);

    useEffect(() => {
        const id = window.setInterval(() => {
            setRefreshCount(r => r + 1);
        }, 4000);

        return () => {
            window.clearInterval(id);
        }

    }, []);


    const doRefresh = useCallback(() => {
        setRefreshCount(r => r + 1);
    }, [setRefreshCount]);


    return <div className="player-info-page">
        <h2>在线玩家 - 当前{players.length}人在线 <Button onClick={doRefresh}>刷新</Button></h2>

        <Row gutter={[16, 16]} align={'middle'} justify={'start'}>
            {
                players.map((player) => {
                    return <Col key={player.name} xs={12} sm={12} md={8} lg={6} xl={6} xxl={4}>
                        <OnlinePlayerInfo player={player} />
                    </Col>
                })
            }
        </Row>

    </div>

}

export default OnlinePlayersPage;
