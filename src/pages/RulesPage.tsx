
import { List, Typography } from 'antd'
import React from 'react'

import "./RulesPage.css"

export default function RulesPage() {

    const data = [
        "恶意破坏他人建筑的行为，由建筑者决定处罚方式，最高十年封号",
        "未经许可拿他人东西的行为，由失主决定处罚方式，最高十年封号",
        "使用外挂或其它不被允许的辅助功能的，直接永久封号！",
        "其它未定义的、引起其它玩家不满的行为，由所有玩家投票决定处罚方式"
    ];

  return (
    <>
    <List 
    className='rules-page'
    header = {<h2>PaperCard四大基本条例</h2>}
    footer = {"以上基本条例的解释权归服主所有"}
    size= {"large"}
    dataSource={data}
     renderItem={(item, index) => (
        <List.Item>
        {index + 1} {item}
        </List.Item>
      )}/>
    </>
  )
}
